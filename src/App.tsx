import * as React from 'react';
import * as s from './App.scss';

import { Moment } from 'moment';
import HourSelector, { FORMAT } from './components/HourSelector';

class App extends React.Component {
  public state = {
    date: '11/23/2018 8am',
  };

  public handleChange = (date: Moment) => {
    this.setState({ date: date.format(FORMAT) });
  };

  public render() {
    return (
      <div className={s.app}>
        <h1>Hour Selector Demo</h1>
        <div className={s.uncontrolled}>
          <h2>Uninitialized Component</h2>
          <HourSelector />
        </div>
        <h2>Initialized Component: {this.state.date}</h2>
        <HourSelector initialHour={this.state.date} onChange={this.handleChange} />
      </div>
    );
  }
}

export default App;
