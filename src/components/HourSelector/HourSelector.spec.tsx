import { mount } from 'enzyme';
import * as moment from 'moment';
import * as React from 'react';
import HourSelector, { FORMAT } from './';

describe('HourSelector', () => {
  it('should render without crashing', () => {
    mount(<HourSelector />);
  });

  it('should handle text input', () => {
    const handleChange = jest.fn();
    const value = moment().format(FORMAT);
    mount(<HourSelector onChange={handleChange} />)
      .find('input')
      .simulate('change', {
        currentTarget: {
          value,
        },
      });
    expect(handleChange).toHaveBeenCalledWith(moment(value, FORMAT));
  });

  it('should handle calendar input', () => {
    const handleChange = jest.fn();
    const now = moment();
    const currentHour = now
      .clone()
      .minute(0)
      .second(0)
      .millisecond(0);

    const hourSelector = mount(<HourSelector onChange={handleChange} />);
    hourSelector.find('input').simulate('focus');
    hourSelector.find(`[aria-label="day-${now.date()}"]`).simulate('click');

    expect(handleChange).toHaveBeenCalledWith(currentHour);
  });
});
