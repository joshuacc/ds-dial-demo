import { Moment } from 'moment';
import * as moment from 'moment';
import * as React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import Draggable, { DraggableData } from 'react-draggable';
import { FaCalendarAlt } from 'react-icons/fa';
import { noop } from '../../utils';
import * as s from './HourSelector.scss';

export const FORMAT = 'MM/DD/YYYY ha';

interface Props {
  onChange: (date: Moment) => void;
  initialHour: string;
}
interface State {
  hours: Moment[];
  hoursMap: Map<string, number>;
  selectedHour: string;
}

let instanceCount = 0;

export default class HourSelector extends React.Component<Props, State> {
  public static defaultProps = {
    initialHour: moment().format(FORMAT),
    onChange: noop,
  };

  public instanceId = instanceCount;

  public state = {
    hours: [] as Moment[],
    hoursMap: new Map<string, number>(),
    selectedHour: moment().format(FORMAT),
  };

  private dialContainerRef = React.createRef<HTMLDivElement>();

  constructor(props: Props) {
    super(props);
    instanceCount++;
  }

  public componentDidMount() {
    this.setState(this.prepSelectableHours());
    if (typeof this.props.initialHour === 'string') {
      this.setState({ selectedHour: this.props.initialHour });
    }
  }

  public render() {
    const { dialStyles, notchSectionWidth, draggableX } = this.getStyles();

    const datepickerId = `HourSelector-date-input-${this.instanceId}`;

    return (
      <div className={s.hourSelector}>
        <div className={s.datepickerLine}>
          <DatePicker
            id={datepickerId}
            className={s.input}
            onChange={this.handleDateChange}
            onChangeRaw={this.handleRawChange}
            minDate={moment().subtract(3, 'months')}
            maxDate={moment()}
            dateFormat="L"
            value={this.state.selectedHour}
            title="Choose a date"
          />
          <label htmlFor={datepickerId} className={s.label}>
            <FaCalendarAlt />
          </label>
        </div>
        <div className={s.dialContainer} ref={this.dialContainerRef}>
          <div className={s.dialBackground}>
            <Draggable
              axis="x"
              grid={[notchSectionWidth, notchSectionWidth]}
              bounds={{ right: dialStyles.width, left: 0, top: 0, bottom: 0 }}
              position={{ x: draggableX, y: 0 }}
              onStop={this.handleDragStop(notchSectionWidth)}
            >
              <div className={s.dial} style={dialStyles}>
                {this.state.hours.map(h => (
                  <button
                    value={h.format(FORMAT)}
                    key={h.unix()}
                    onClick={this.handleNotchClick}
                    className={s.hourNotch}
                  />
                ))}
              </div>
            </Draggable>
          </div>
          <div className={s.pointer} />
          <div className={s.shadow} />
        </div>
      </div>
    );
  }

  private updateDate = (rawDate: Moment | string) => {
    const date = moment(rawDate, FORMAT);
    this.setState({ selectedHour: date.format(FORMAT) });
    this.props.onChange(date);
  };

  private handleDateChange = (newDate: Moment) => {
    const prevDate = moment(this.state.selectedHour, FORMAT);
    const newDateWithCurrentHour = newDate.clone().hour(prevDate.hour());
    this.updateDate(newDateWithCurrentHour);
  };

  private handleRawChange = (event: React.SyntheticEvent<HTMLInputElement>) => {
    this.updateDate(event.currentTarget.value);
  };

  private handleNotchClick = (event: React.SyntheticEvent<HTMLButtonElement>) => {
    this.updateDate(event.currentTarget.value);
  };

  private handleDragStop = (notchSectionWidth: number) => (
    _event: MouseEvent,
    data: DraggableData
  ) => {
    const { x, deltaX } = data;
    // If there is no delta, the handler was triggered by a click rather than a drag
    if (deltaX === 0) {
      return;
    }

    const reverseIndex = Math.round(x / notchSectionWidth);

    const { hours } = this.state;
    const index = hours.length - reverseIndex - 1;
    const hour = hours[index];
    if (hour) {
      this.updateDate(hour);
    }
  };

  private getStyles = () => {
    const NOTCH_WIDTH = 8;
    const { selectedHour, hoursMap } = this.state;
    const currentIndex = hoursMap.get(selectedHour);

    const dialContainer = this.dialContainerRef.current;
    const dialContainerWidth = dialContainer ? dialContainer.clientWidth : 0;
    const notchSectionWidth = dialContainerWidth / 12;
    const width = notchSectionWidth * this.state.hours.length;
    const dialPadding = dialContainerWidth / 2;

    let right = 0;

    let draggableX = 0;

    if (currentIndex !== undefined) {
      right = -(notchSectionWidth / 2 + NOTCH_WIDTH / 2);
      draggableX = width - (currentIndex + 1) * notchSectionWidth;
    }

    return {
      dialStyles: { right, width, paddingRight: dialPadding, paddingLeft: dialPadding },
      draggableX,
      notchSectionWidth,
    };
  };

  private prepSelectableHours = () => {
    const now = moment();
    const threeMonthsAgo = moment(now).subtract(3, 'months');

    const hours: Moment[] = [];
    const hoursMap = new Map<string, number>();

    let index = 0;
    for (
      let hour = threeMonthsAgo
        .clone()
        .minutes(0)
        .seconds(0)
        .milliseconds(0);
      hour.isBefore(now);
      hour = moment(hour).add(1, 'hour')
    ) {
      hours.push(hour);
      hoursMap.set(hour.format(FORMAT), index);
      index++;
    }

    return { hours, hoursMap };
  };
}
